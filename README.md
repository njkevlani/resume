# Résumé
- [njkevlani.github.io/resume/](https://njkevlani.github.io/resume/)
- [njkevlani.gitlab.io/resume/](https://njkevlani.gitlab.io/resume/)
# About
This resume is created using [hackmyreume](https://github.com/hacksalot/HackMyResume) and name of theme is  [Stackoverflow](https://themes.jsonresume.org/stackoverflow).

Exported to plain HTML, CSS (I have tinkered around with HTML after exporting to HTML for some custom changes) and hosted on Github with [GitHub Pages](https://pages.github.com/) and on GitLab with [GitLab Pages](https://about.gitlab.com/product/pages/).
